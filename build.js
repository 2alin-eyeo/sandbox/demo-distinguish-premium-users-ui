const { execSync } = require("child_process");
const fs = require("fs");
const { join } = require("path");

const repoURL = "https://gitlab.com/2alin-eyeo/abp/adblockplusui.git";
const repoName = "adblockplusui";
const branchName = "ui_1139";

function setupRepo() {
  let repoIsCloned = false;
  if (fs.existsSync(repoName)) {
    try {
      const output = execSync("cd adblockplusui && git remote --v");
      const repoURLMatched = output
        .toString()
        .match(/origin\s+(\S+)\s+\(fetch\)/)[1];
      if (repoURLMatched === repoURL) {
        repoIsCloned = true;
      } else {
        throw new Error("unrecognized repo URL");
      }
    } catch (error) {
      fs.rmSync(repoName, { recursive: true });
    }
  }

  if (!repoIsCloned) {
    execSync(`git clone ${repoURL} ${repoName}`);
  }
}

console.log("### setting up repo ###");
setupRepo();
execSync(
  `cd ${repoName} && git checkout ${branchName} && npm run submodules:update`,
  { stdio: "inherit" }
);

console.log("### installing dependencies ###");
execSync(`cd ${repoName} && npm install`, { stdio: "inherit" });

console.log("### bundling mocks ###");
execSync(
  `cd ${repoName} &&
npm run $ bundle.mocks &&
npm run $ bundle.desktop-options &&
npm run $ bundle.popup
`,
  { stdio: "inherit" }
);

console.log("### preparing dist directory ###");
fs.rmSync("public", { force: true, recursive: true });
fs.mkdirSync("public");

const coreLocales = [
  "ar",
  "de",
  "el",
  "en-US",
  "es",
  "fr",
  "hu",
  "it",
  "ja",
  "ko",
  "nl",
  "pl",
  "pt-BR",
  "ru",
  "tr",
  "zh-CN",
];

const pathsToCopy = [
  {
    base: "./",
    paths: ["index.html", "main.js", "style.css"],
  },
  {
    base: "adblockplusui",
    paths: [
      ...coreLocales.map((locale) => join("locale", locale.replace("-", "_"))),
      "data",
      "ext",
      "mocks/data",
      "mocks/dist",
      "mocks/background.html",
      "polyfill.js",
      "skin",
      "desktop-options.js",
      "desktop-options.html",
      "popup.js",
      "popup.html",
    ],
  },
];

for (const { base, paths } of pathsToCopy) {
  paths.forEach((path) => {
    fs.cpSync(join(base, path), join("public", path), {
      force: true,
      recursive: true,
    });
  });
}

// repo mock fixes

const polyfillFilePath = "public/polyfill.js";
const polyfillFile = fs.readFileSync(polyfillFilePath);
fs.writeFileSync(
  polyfillFilePath,
  polyfillFile.toString().replaceAll(".top.location", ".location")
);

const backgroundFilePath = "public/mocks/dist/background.js";
const backgroundFile = fs.readFileSync(backgroundFilePath);
fs.writeFileSync(
  backgroundFilePath,
  backgroundFile.toString().replaceAll("top.location", "parent.location")
);
