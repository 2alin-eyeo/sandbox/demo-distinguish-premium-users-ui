const $ = (query, element) => document.querySelector(query, element);
const coreLocales = [
  "ar",
  "de",
  "el",
  "en-US",
  "es",
  "fr",
  "hu",
  "it",
  "ja",
  "ko",
  "nl",
  "pl",
  "pt-BR",
  "ru",
  "tr",
  "zh-CN",
];

const iframe = $("iframe");
const pageSelector = $("#page-selector");
const localeSelector = $("#locale-selector");
const isPremiumCheckbox = $("#is-premium");

function updateIframe() {
  const page = pageSelector.value;
  const parameters = new URLSearchParams();

  const locale = localeSelector.value;
  if (locale !== "en-US") {
    parameters.set("locale", locale);
  }
  
  const isPremium = isPremiumCheckbox.checked;
  parameters.set("premiumIsActive", isPremium);

  iframe.src = `${page}.html${
    [...parameters.keys()].length ? `?${parameters.toString()}` : ""
  }`;
}

/*
async function isPremiumActivated() {
  return await iframe.contentWindow.browser.runtime.sendMessage({
    type: "prefs.get",
    key: "premium_is_active",
  });
}

async function togglePremium(value) {
  if (typeof value !== "boolean")
    value = !(await isPremiumActivated());
  browser.runtime.sendMessage({
    type: "prefs.set",
    key: "premium_is_active",
    value,
  });
}
*/

function init() {
  // setup

  coreLocales.forEach((locale) => {
    const option = document.createElement("option");

    if (locale === "en-US") {
      option.selected = true;
    }

    option.value = locale;
    option.textContent = locale;
    localeSelector.append(option);
  });

  updateIframe();

  // listeners

  pageSelector.addEventListener("change", (event) => {
    updateIframe();
  });

  localeSelector.addEventListener("change", (event) => {
    updateIframe();
  });

  isPremiumCheckbox.addEventListener("change", (event) => {
    updateIframe();
  })
}

init();
